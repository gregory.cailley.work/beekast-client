export class GameStatus {
  static readonly COUNTING = 'counting';
  static readonly RUNNING = 'running';
  static readonly WAITING = 'waiting';
  static readonly FINISHED = 'finished';

  constructor(public start: boolean, public winner: Boolean) {}
}
