
export class Game {


  constructor(
    public timer: number,
    public otherPlayerProfilId: string,
    public otherPlayerUsername: string,
  ) {}
}
