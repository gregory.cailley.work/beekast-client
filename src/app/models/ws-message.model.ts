export class WsMessage<T> {
  constructor(public type:string, public data : T) {}
}
