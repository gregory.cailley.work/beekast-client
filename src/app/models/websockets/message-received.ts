export class MessageReceived {
 static readonly GAME_FINISH='EndGame'; // message when game is finished
 static readonly GAME_CONFIGURATION='GameConfiguration'; // message when we have the configuration
 static readonly GAME_START='StartGame';  // message when the server said game stat now
 static readonly PLAYER_CREATED='PlayerCreated'; // message when confirmation of creation
}

