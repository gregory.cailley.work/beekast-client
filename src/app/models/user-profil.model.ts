import { AvatarProfil } from './avatar-profil.model';

export class Player {
  constructor(
    public username: string = null,
    public avatarProfil: AvatarProfil = null
  ) {}
}
