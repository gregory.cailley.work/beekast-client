import { Injectable } from '@angular/core';
import { Player } from '../models/user-profil.model';
import { AvatarService } from '../services/technical/avatar.service';

@Injectable({ providedIn: 'root' })
export class PlayerFactory {
  constructor(private _avatarService: AvatarService) {}

  /**
   * create a player based on username and avatar id.
   *
   * @param username opponent username
   * @param avatarId opponent avatar ID
   * @returns new player
   */
  createPlayer(username: string, avatarId: string) {
    const opponent = new Player();
    opponent.username = username;
    opponent.avatarProfil = this._avatarService.fetchAvatarProfil(
      avatarId
    );
    return opponent;
  }
}
