import { DOCUMENT } from '@angular/common';
import {
  AfterViewInit,
  Component,
  ElementRef,
  HostBinding,
  HostListener,
  Inject,
  OnInit,
  Renderer2,
} from '@angular/core';
import { Router } from '@angular/router';
import { combineLatest, Observable, timer } from 'rxjs';
import { map, take } from 'rxjs/operators';

import { GameStatus } from 'src/app/models/game-status.model';
import { Game } from 'src/app/models/game.model';
import { Player } from 'src/app/models/user-profil.model';
import { GameService } from 'src/app/services/business/game.service';
import { SessionService } from 'src/app/services/technical/session.service';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.scss'],
})
export class GameComponent implements OnInit, AfterViewInit {
  public player: Player;
  public playerStatus: string;

  public opponent: Player;
  public opponentStatus: string;

  public playerMessage: string;
  public timer$: Observable<number>;
  public count: number;

  public gameStatus: string; // tell us if the game is started.

  constructor(
    protected sessionService: SessionService,
    protected gameService: GameService,
    private router: Router,
    private _element: ElementRef,
    private _renderer: Renderer2
  ) {}

  ngOnInit(): void {
    this.gameStatus = GameStatus.WAITING; // game is waiting for configuration and opponent

    this._initializePlayerOrLeave();
    this._initializeOpponentAndGameConfiguration();
    this._initalizeGameStatusObservable();
  }

  ngAfterViewInit() {
    let el = this._element.nativeElement.parentNode;
    this._renderer.setAttribute(el, 'class', 'battlefield' );
  }

  /**
   * Initialize the user on the component from the session.
   */
  private _initializePlayerOrLeave() {
    this.player = this.sessionService.getMyLastProfil();

    // if the user have not player account in the session we redirect to the login page
    // TOOD - the guard process is better for this action.
    if (!this.player) {
      this.router.navigate(['/inscription']);
    }
  }

  /**
   * We wait for both opponent and game configuration to be provided by the server.
   * we both information we can set the stage and wait for the signal.
   */
  private _initializeOpponentAndGameConfiguration() {
    combineLatest([
      this.gameService.opponentProfil$,
      this.gameService.gameConfiguration$,
    ]).subscribe((data) => {
      console.debug('Game informations received,', data);
      this.opponent = data[0];
      this.count = (<Game>data[1]).timer;
    });
  }

  /**
   * We observe the game status managed by the server
   * if the game.start is true we can start the count down provided by the game configuration.
   * if the game.start is false someone is the winner, let's have a look in winner attribut
   * niveau de debug
   */
  private _initalizeGameStatusObservable() {
    this.gameService.gameServerStatus$.subscribe((game) => {
      if (game.start != undefined) {
        if (game.start) {
          console.debug('Game is starting.');
          this.startCountDown();
        } else {
          console.debug('Game is finished. are you the winner ?', game.winner);
          game.winner ? this.doPlayerWin() : this.doOpponentWin();
        }
      }
    });
  }

  /**
   * method which strat the count down.
   */
  startCountDown() {
    // timer dealer when the opponent is here we start the timer.
    this.timer$ = timer(0, 1000).pipe(
      take(this.count),
      map(() => {
        if (this.count <= 1) {
          this.gameStatus = GameStatus.RUNNING;
        }
        return --this.count;
      })
    );
    this.gameStatus = GameStatus.COUNTING;
  }

  /**
   * We listen for an activity only between COUNTING and RUNNING
   * we send the player action.
   * @param event
   */
  @HostListener('document:touchstart', ['$event'])
  @HostListener('document:keypress', ['$event'])
  handleKeyboardEvent(event: KeyboardEvent) {
    switch (this.gameStatus) {
      case GameStatus.COUNTING:
      case GameStatus.RUNNING:
        console.debug(
          'user action detected with the game status:',
          this.gameStatus
        );
        this.gameService.sendAction();
        break;
      default:
        break;
    }
  }

  doOpponentWin() {
    this.playerStatus = 'loser';
    this.opponentStatus = 'winner';
    this.playerMessage = 'Désolé vous avez perdu';
    this.gameStatus = GameStatus.FINISHED;
  }

  doPlayerWin() {
    this.opponentStatus = 'loser';
    this.playerStatus = 'winner';
    this.playerMessage = 'Bravo vous avez gagné';
    this.gameStatus = GameStatus.FINISHED;
  }

  ngOnDestroy() {
    let el = this._element.nativeElement.parentNode;
    this._renderer.setAttribute(el, 'class', '' );
  }
}
