import { NO_ERRORS_SCHEMA, Type } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { PlayerFactory } from 'src/app/factories/player.factory';
import { SessionService } from 'src/app/services/technical/session.service';
import { MenuComponent } from '../menu/menu.component';

import { GameComponent } from './game.component';

describe('GameComponent', () => {
  let component: GameComponent;
  let fixture: ComponentFixture<GameComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MenuComponent, GameComponent ],
      imports: [RouterTestingModule],
      schemas: [NO_ERRORS_SCHEMA],
    })
    .compileComponents();
  });

  beforeEach(() => {
    // create session user
    // TODO testClass master needed
    const sessionService: SessionService = TestBed.inject(
      SessionService as Type<SessionService>
    );
    const playerFactory: PlayerFactory = TestBed.inject(
      PlayerFactory as Type<PlayerFactory>
    );
    sessionService.setMyProfil(playerFactory.createPlayer('myUsernmae', '1'));

    fixture = TestBed.createComponent(GameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
