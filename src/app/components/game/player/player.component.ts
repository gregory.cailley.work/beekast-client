import { Component, Input, OnInit } from '@angular/core';
import { Player } from 'src/app/models/user-profil.model';

import {
  trigger,
  state,
  style,
  animate,
  transition,
  // ...
} from '@angular/animations';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.scss'],
  animations: [
    trigger('playWinner', [
      state(
        'winner-left',
        style({
          opacity: 1,
          transform: 'translate(200%, 0px)',
          zIndex: 2,
        })
      ),
      transition('* => winner-left', [animate('0.5s')]),

      state(
        'winner-right',
        style({
          opacity: 1,
          transform: 'translate(-200%, 0px)',
          zIndex: 2,
        })
      ),
      transition('* => winner-right', [animate('0.5s')]),


      state(
        'loser-right',
        style({
          transform: 'translate(-200%, 0px)',
          opacity: 0.1,
          zIndex: 1,
        })
      ),
      transition('* => loser-right', [animate('1s')]),

      state(
        'loser-left',
        style({
          transform: 'translate(200%, 0px)',
          opacity: 0.1,
          zIndex: 1,
        })
      ),
      transition('* => loser-left', [animate('1s')]),
    ]),
  ],
})
export class PlayerComponent implements OnInit {
  @Input() player: Player;
  @Input() side = 'left';
  @Input() status: boolean;

  constructor() {}

  ngOnInit(): void {}

}
