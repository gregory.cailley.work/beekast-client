import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';

import { AvatarProfil } from 'src/app/models/avatar-profil.model';
import { Player } from 'src/app/models/user-profil.model';
import { GameService } from 'src/app/services/business/game.service';
import { AvatarService } from 'src/app/services/technical/avatar.service';
import { SessionService } from 'src/app/services/technical/session.service';

@Component({
  selector: 'app-inscription',
  templateUrl: './inscription.component.html',
  styleUrls: ['./inscription.component.scss'],
})
export class InscriptionComponent implements OnInit {
  public userProfil: Player;
  public avatarProfils: AvatarProfil[] = [];
  public userProfil$: Observable<Player>;

  constructor(
    private _gameSercice: GameService,
    private _avatarService: AvatarService,
    protected sessionService: SessionService,
    public router: Router
  ) {

  }

  public ngOnInit() {
    // load avatar lib
    this.avatarProfils = this._avatarService.getAvatarProfils();
    // if we already have a account we update the form.
    const sessionPlayer = this.sessionService.getMyLastProfil();
    if (sessionPlayer) {
      // we detach the object so we are able NOT to update the session object directly
      this.userProfil = { ...sessionPlayer };
    } else {
      this.userProfil = new Player();
    }
  }

  /**
   * method to connect the user profil with the server.
   */
  doConnect() {
    // we detach the userProfil form the ViewModel otherwise any change on the View we change in the data store.
    this._gameSercice.createPlayer({ ...this.userProfil });
    // redirect to the game scene.
    this.router.navigate(['/game']);
  }

  /**
   *
   * method to set the avatar selected.
   * @param avatarProfil Profil select
   */
  doSelect(avatarProfil: AvatarProfil) {
    this.userProfil.avatarProfil = avatarProfil;
  }

  get username(): string {
    return this.userProfil.username;
  }
  set username(username: string) {
    this.userProfil.username = username;
  }
  get avatarProfil(): AvatarProfil {
    return this.userProfil.avatarProfil;
  }
  set avatarProfil(avatarProfil: AvatarProfil) {
    this.userProfil.avatarProfil = avatarProfil;
  }
}
