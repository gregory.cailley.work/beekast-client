import { NO_ERRORS_SCHEMA } from '@angular/core';
import {
  ComponentFixture,
  fakeAsync,
  TestBed,
  tick,
} from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';

import { InscriptionComponent } from './inscription.component';

describe('InscriptionComponent', () => {
  let component: InscriptionComponent;
  let fixture: ComponentFixture<InscriptionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [InscriptionComponent],
      imports: [RouterTestingModule, FormsModule],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InscriptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have a input for the username', () => {
    const compiled = fixture.nativeElement;
    // Controller
    expect(component.username).toBeNull();

    //View
    const usernameInputEl = compiled.querySelector('input');
    // input available and null by default
    expect(usernameInputEl).not.toBeNaN();
    // with alt not null
    expect(usernameInputEl.getAttribute('alt')).not.toBeNull();
  });

  it('should have a 3 profils to select', () => {
    const compiled = fixture.nativeElement;
    // Controller
    // need 3 profils
    expect(component.avatarProfils.length).toEqual(3);

    const selectInputEl = compiled.querySelectorAll('.profil');
    expect(selectInputEl).not.toBeNull();
    expect(selectInputEl.length).toEqual(3);
  });

  it('should display "play" button', fakeAsync(() => {
    const compiled = fixture.nativeElement;

    // check initial button visibility
    expect(compiled.querySelectorAll('button').length).toEqual(0);

    // add username
    component.username = 'xx';
    fixture.detectChanges();
    tick();

    const usernameInputEl = compiled.querySelector('input');
    expect(usernameInputEl.value).toEqual(component.username);
    // check button visibility after setting username
    expect(compiled.querySelectorAll('button').length).toEqual(0);

    // select profil using doSelect
    expect(component.avatarProfil).toBeNull();
    component.doSelect(component.avatarProfils[1]);
    fixture.detectChanges();
    tick();

    expect(component.avatarProfil).not.toBeNull();
    // check button visibility after setting username AND profil
    expect(compiled.querySelectorAll('button').length).toEqual(1);
  }));
});
