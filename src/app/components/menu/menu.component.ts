import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Player } from 'src/app/models/user-profil.model';
import { ServerService } from 'src/app/services/business/server.service';
import { SessionService } from 'src/app/services/technical/session.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
})
export class MenuComponent implements OnInit {
  public userProfil$: Observable<Player>;
  public isConnect$: Observable<boolean>;

  constructor(
    protected sessionService: SessionService,
    protected serverService: ServerService
  ) {}

  public ngOnInit() {

    this.userProfil$ = this.sessionService.player$;
    this.isConnect$ = this.serverService.serverStatus$;
  }

}
