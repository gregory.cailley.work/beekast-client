import { LayoutModule } from '@angular/cdk/layout';
import { waitForAsync, ComponentFixture, TestBed } from '@angular/core/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatToolbarModule } from '@angular/material/toolbar';
import { Type } from '@angular/core';

import { MenuComponent } from './menu.component';
import { SessionService } from 'src/app/services/technical/session.service';
import { Player } from 'src/app/models/user-profil.model';
import { PlayerFactory } from 'src/app/factories/player.factory';

describe('MenuComponent', () => {
  let component: MenuComponent;
  let fixture: ComponentFixture<MenuComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [MenuComponent],
        imports: [
          NoopAnimationsModule,
          LayoutModule,
          MatButtonModule,
          MatIconModule,
          MatListModule,
          MatSidenavModule,
          MatToolbarModule,
        ],
      }).compileComponents();
    })
  );

  beforeEach(() => {
    const sessionService: SessionService = TestBed.inject(
      SessionService as Type<SessionService>
    );
    const playerFactory: PlayerFactory = TestBed.inject(
      PlayerFactory as Type<PlayerFactory>
    );
    // set user session
    sessionService.setMyProfil(playerFactory.createPlayer('myUsernmae', '1'));
    fixture = TestBed.createComponent(MenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should compile', () => {
    expect(component).toBeTruthy();
  });

  it('should have player already set using the sesssion service', () => {
    const compiled = fixture.nativeElement;
    const linkEl = compiled.querySelector('img');
    expect(linkEl).toBeDefined();
    expect(linkEl.getAttribute('src')).not.toBeNull();
  });
});
