import { Injectable, OnDestroy } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Player } from 'src/app/models/user-profil.model';

@Injectable({
  providedIn: 'root',
})
export class SessionService implements OnDestroy {
  private _player: BehaviorSubject<Player> = new BehaviorSubject(
    null
  );
  public player$: Observable<Player> = this._player.asObservable();
  /**
   * set the play profil and notify subscribers.
   * @param userProfil player profil.
   */
  setMyProfil(userProfil: Player) {
    this._player.next(userProfil);
  }
  /**
   * get last profil created.
   * @returns last user profil created.
   */
  getMyLastProfil(): Player {
    return this._player.getValue();
  }

  public ngOnDestroy() {
    this._player.unsubscribe();
  }
}
