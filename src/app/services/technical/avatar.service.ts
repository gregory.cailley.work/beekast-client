import { Injectable } from '@angular/core';
import { AvatarProfil } from 'src/app/models/avatar-profil.model';

@Injectable({
  providedIn: 'root'
})
export class AvatarService {

  private _avatarProfils: AvatarProfil[] = [
    {
      id: '1',
      image: 'assets/avatar/1.png',
    },
    {
      id: '2',
      image: 'assets/avatar/2.png',
    },
    {
      id: '3',
      image: 'assets/avatar/3.png',
    },
  ];

  constructor() { }

  getAvatarProfils() {
    return this._avatarProfils;
  }

  /**
   * getter avatar by Id.
   * @param id avatar Id to find
   * @returns AvatarProfil
   */
  fetchAvatarProfil(id: string) {
    return this._avatarProfils.find((avatar) => avatar.id === id);
  }
}
