import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject, Subscriber } from 'rxjs';
import { io } from 'socket.io-client/build/index';
import { GameStatus } from 'src/app/models/game-status.model';
import { Game } from 'src/app/models/game.model';

import { environment } from '../../../environments/environment';
export const WS_ENDPOINT = environment.wsEndpoint;

import { Player } from '../../models/user-profil.model';
import { WsMessage } from '../../models/ws-message.model';
import { MessageSend } from '../../models/websockets/message-send';
import { MessageReceived } from 'src/app/models/websockets/message-received';

@Injectable({
  providedIn: 'root',
})
export class WebSocketService {
  private _socket;
  public messages$: Observable<MessageEvent>;
  private _statusSubject$: Subject<boolean> = new BehaviorSubject<boolean>(
    false
  );
  public status$: Observable<boolean> = this._statusSubject$.asObservable();

  constructor() {
    this._connect();
  }

  /**
   * connect the client to the server.
   *
   * @returns status of the connexion
   */
  private _connect(): void {
    this._socket = this.createSocket();
    this.initObservable();
  }

  /**
   * create client socket object;
   * @returns socket object
   */
  private createSocket() {
    return io(environment.wsEndpoint);
  }

  /**
   * init observable who listen to server message.
   */
  public initObservable = () => {
    this.messages$ = new Observable((observer) => {
      // FONCTIONNAL MESSAGES  ///
      this._socket.on(MessageReceived.PLAYER_CREATED, (message) =>
        this.onRegistered(observer, message)
      );
      this._socket.on(MessageReceived.GAME_CONFIGURATION, (message) =>
        this.onGameconfiguration(observer, message)
      );
      this._socket.on(MessageReceived.GAME_START, (message) =>
        this.onStartGame(observer, message)
      );
      this._socket.on(MessageReceived.GAME_FINISH, (message) =>
        this.onEndGame(observer, message)
      );

      // TECHNICAL MESSAGES  ///
      this._socket.on('connect', (message) =>
        this.onConnectDisconnect(observer, message, true)
      );
      this._socket.on('disconnect', (message) =>
        this.onConnectDisconnect(observer, message, false)
      );
    });
  };

  /**
   * method to close the Subject.
   */
  doCloseConnection() {
    this._socket.complete();
  }

  ////////////////////////////////////////////////
  // Actions when received message from the server
  ////////////////////////////////////////////////
  public onRegistered(observer: Subscriber<any>, message): void {
    observer.next(new WsMessage<string>(MessageReceived.PLAYER_CREATED, message));
  }

  public onGameconfiguration(observer: Subscriber<any>, message): void {
    observer.next(new WsMessage<Game>(MessageReceived.GAME_CONFIGURATION, message));
  }

  public onEndGame(observer: Subscriber<any>, message): void {
    observer.next(new WsMessage<GameStatus>(MessageReceived.GAME_FINISH, message));
  }

  public onStartGame(observer: Subscriber<any>, message): void {
    observer.next(new WsMessage<GameStatus>(MessageReceived.GAME_START, message));
  }
  /**
   * Method for connect and disconnect event from the server.
   *
   * @param observer Subcription
   * @param message message
   */
  onConnectDisconnect(
    observer: Subscriber<any>,
    message,
    status: boolean
  ): void {
    observer.next(message);
    this._statusSubject$.next(status);
  }

  ////////////////////////////////////////////////
  // Messages emit to server
  ////////////////////////////////////////////////

  /**
   * Method to call when the profil is created and ready to play.
   *
   * @param player UserProfil to play with.
   */
  emitCreatePlayer(player: Player): void {
    this._socket.emit(MessageSend.CREATE_PLAYER, JSON.stringify(player));
  }

  /**
   * Send action to the server.
   */
  emitActionPlayer(): void {
    this._socket.emit(MessageSend.ACTION_PLAYER, null);
  }
}
