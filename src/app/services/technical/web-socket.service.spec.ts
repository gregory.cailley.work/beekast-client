import { TestBed } from '@angular/core/testing';
import { environment } from 'src/environments/environment';

import { WebSocketService } from './web-socket.service';

describe('WebSocketService', () => {
  let service: WebSocketService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(WebSocketService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('message should be initialized after connect', (done: DoneFn) => {
    expect(service.messages$).not.toBeUndefined();
    expect(service.status$).not.toBeUndefined();
    done();
  });

  // TODO see how to mock the server
  // it('connect to server KO' ,  (done: DoneFn)  => {
  //   environment.wsEndpoint = "ws://xyz";
  //   expect(service.connect()).toBeFalse();
  //   done();
  // });

  // TODO see how to mock the server
  // it('connect to server OK (mock server)' ,  (done: DoneFn)  => {
  //   expect(service.connect()).toBeTrue();
  //   done();
  // });
});
