import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { WebSocketService } from '../technical/web-socket.service';

@Injectable({
  providedIn: 'root'
})
export class ServerService {
  public serverStatus$: Observable<boolean>;

  constructor(private _webSocketService: WebSocketService) {
    this.serverStatus$ = this._webSocketService.status$;
  }
}
