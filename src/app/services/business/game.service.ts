import { Injectable, OnDestroy } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { Game } from '../../models/game.model';
import { Player } from '../../models/user-profil.model';
import { WsMessage } from '../../models/ws-message.model';
import { WebSocketService } from '../technical/web-socket.service';
import { GameStatus } from '../../models/game-status.model';
import { SessionService } from '../technical/session.service';
import { AvatarService } from '../technical/avatar.service';
import { PlayerFactory } from 'src/app/factories/player.factory';
import { MessageReceived } from 'src/app/models/websockets/message-received';

@Injectable({
  providedIn: 'root',
})
export class GameService implements OnDestroy {
  /////// Game server part
  private _gameServerStatusSubject: BehaviorSubject<GameStatus> = new BehaviorSubject(
    new GameStatus(undefined, undefined)
  );
  public gameServerStatus$: Observable<GameStatus> = this._gameServerStatusSubject.asObservable();

  /////// Game Configuration part
  private _gameConfigurationSubject: BehaviorSubject<Game> = new BehaviorSubject(
    null
  );
  public gameConfiguration$: Observable<Game> = this._gameConfigurationSubject.asObservable();

  /////// Opponent part
  private _opponentSubject: Subject<Player> = new Subject();
  public opponentProfil$: Observable<Player> = this._opponentSubject.asObservable();

  constructor(
    private _webSocketService: WebSocketService,
    private _sessionService: SessionService,
    private _playerFactory:PlayerFactory,
  ) {
    // listen to websocket message and do thing.
    this._webSocketService.messages$.subscribe(
      (message: MessageEvent<WsMessage<unknown>>) => {
        if (message) {
          switch (message.type) {
            case MessageReceived.GAME_CONFIGURATION:
              this.onConfiguration(<Game>(<unknown>message.data));
              break;
            case MessageReceived.GAME_START:
              this.onStartGame();
              break;
            case MessageReceived.GAME_FINISH:
              this.onEndGame(<GameStatus>(<unknown>message.data));
              break;
              case MessageReceived.PLAYER_CREATED:
                console.log('player created on server');
                break;
            default:
              console.error('Unknwon case ' + message.type);
              break;
          }
        }
      }
    );
  }

  /**
   * Method to set the game server configuration and notify subscribers about the game configuration and the opponnent.
   *
   * @param gameServer game server configuration.
   */
  onConfiguration(gameServer: Game) {
    // set game
    this._gameConfigurationSubject.next(gameServer);
    this.initializeOpponentProfil(
      gameServer.otherPlayerUsername,
      gameServer.otherPlayerProfilId
    );
  }

  onStartGame() {
    const gameStatus = new GameStatus(true, undefined);
    this._gameServerStatusSubject.next(gameStatus);
  }

  onEndGame(status: GameStatus) {
    const gameStatus: GameStatus = {
      ...this._gameServerStatusSubject.getValue(),
    };
    gameStatus.start = false;
    gameStatus.winner = status.winner;
    this._gameServerStatusSubject.next(gameStatus);
  }

  /**
   * Method to notify the server for the player profil creation and store it in the local data base.
   */
  createPlayer(player: Player) {
    this._gameServerStatusSubject.next(new GameStatus(undefined, undefined));
    this._webSocketService.emitCreatePlayer(player);
    // save my profil to the global store could be localstorage, cookie, redux...
    this._sessionService.setMyProfil(player);
  }

  /**
   * Send action to the server.
   *
   */
  sendAction() {
    this._webSocketService.emitActionPlayer();
  }

  /**
   * initialize the opponent player and notify subscribers.
   *
   * @param opponentUsername username of opponent
   * @param opponentAvatarId avata ID of opponent
   */
  initializeOpponentProfil(opponentUsername: string, opponentAvatarId: string) {
    const opponent = this._playerFactory.createPlayer(opponentUsername, opponentAvatarId);
    this._opponentSubject.next(opponent);
  }

  public ngOnDestroy() {
    this._opponentSubject.unsubscribe();
    this._gameConfigurationSubject.unsubscribe();
    this._gameServerStatusSubject.unsubscribe();
  }
}
